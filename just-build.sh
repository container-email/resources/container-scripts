#!/bin/sh

# called inside aport folder
#NME="builder"

if [ ! -f APKBUILD ]; then
  echo >&2 "No APKBUILD file to build"
  exit 2
fi

echo "Building ..."
echo "Arch is: $(uname -m)"
abuild checksum
abuild -A
abuild -rK -P /tmp/pkg
