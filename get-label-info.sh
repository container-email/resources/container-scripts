#!/bin/sh
if [ -z "$DOCKER_IMAGE" ]; then
  LABEL_IMAGE="${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_SHA}"
else
  LABEL_IMAGE="$DOCKER_IMAGE"
fi

echo "Getting Label info for : $LABEL_IMAGE"
echo
docker image pull "$LABEL_IMAGE"

VER=$(docker container run --rm "$LABEL_IMAGE" cat /etc/alpine-release)
echo "built with alpine version $VER"
echo "ALP_VER=$VER" >build.env

if [ -n "$APK" ]; then
  echo "checking package versions for $APK"
  AVERS=$(docker container run --rm "$LABEL_IMAGE" cat /etc/apkvers)
  echo "Found:"
  echo "$AVERS"
  AVERS=$(echo "${AVERS}" | xargs)
  echo "APKVERS=$AVERS" >>build.env
  R=$(echo "$AVERS" | cut -d' ' -f1)
  P=$(echo "$R" | sed -E "s/(.*)-.*-r.*/\1/")
  R=$(echo "$R" | sed -E "s/.*-(.*-r[0-9])/\1/")
  echo "Suggested tag for ${P}: ${R}-alpine${VER}-r0"
  echo "Alt date  tag for ${P}: $(date +%d%M%y)-alpine${VER}-r0"
else
  echo "no package versions to check"
fi

INFO=README.md

if [ -f "$INFO" ]; then
  echo "Reading $INFO..."
  echo "NAME=$(head -1 README.md | cut -d' ' -f2)" >>build.env
  DESC1="$(head -2 README.md | tail -n1 | sed 's/[][()]//g')"
  DESC2="$(head -3 README.md | tail -n1 | sed 's/[][()]//g')"
  echo "DESC=$DESC1 $DESC2" >>build.env
else
  echo "$INFO not found"
fi
