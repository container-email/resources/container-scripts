#!/usr/bin/env python3
from argparse import ArgumentParser
from ftplib import FTP
from json import loads
from urllib import error, request

from bs4 import BeautifulSoup
from natsort import natsorted

# docker is only used for cat file and alpine
try:
    from docker import from_env
except ImportError:
    pass

agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) \
        AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"


def catFile(IMG, FILE):
    try:
        client = from_env()
        res_ver = client.containers.run(IMG, "cat " + FILE).decode("utf-8").strip()
    except Exception as ex:
        ex = ex
        res_ver = "not.found"
    return res_ver


def openURI(theURI, timeo=30):
    head = {
        "User-Agent": agent
        }
    requ = request.Request(theURI, None, headers=head)
    try:
        return_data = request.urlopen(requ, timeout=timeo)
    except error.HTTPError as e:
        if hasattr(e, "reason"):
            raise SystemExit("Failed to reach a server, does URL exist: " + str(e.reason))
        elif hasattr(e, "code"):
            raise SystemExit("The server could not fulfill the request: " + e.code)
        else:
            raise SystemExit("unknown error")
    return return_data


def checkList(name, uri, list):
    if not list:
        raise SystemExit("No " + name + " found at " + uri)
    return


def stripPrefix(version):
    if version.startswith(("-", ".", "v")):
        version = version[1:]
    return version


def checkNum(version):
    if version[0].isnumeric() or version[1].isnumeric():
        return True
    return False


def getDockerTag(URI):
    DOCKURI = "https://registry.hub.docker.com/v1/repositories/"
    tags = openURI(DOCKURI + URI + "/tags")

    raw_data = tags.read()
    json_data = loads(raw_data.decode("utf-8"))

    lines = []
    for line in json_data:
        ver = line["name"]
        if ver != "latest" and ver != "docker-hub":
            lines.append(ver)

    return natsorted(lines)[-1]


def getAlpineApk(APK):
    CMD = 'sh -c "apk list --no-cache -q ' + APK + ';"'
    IMG = "alpine"
    client = from_env()
    if args.edge:
        IMG = IMG + ":edge"
    result = (
        client.containers.run(IMG, CMD)
        .decode("utf-8")
        .strip(APK + "-")
        .split(" ", 1)[0]
    )
    return result


def getAlpineVer(IMG="alpine"):
    if IMG == "alpine" and args.edge:
        IMG = IMG + ":edge"
    return catFile(IMG, "/etc/alpine-release")


def getGitHash(FILE):
    data = openURI("https://raw.githubusercontent.com/" + FILE)
    return data.getheader("ETag").strip('"')


def getFileHash(IMG):
    return catFile(IMG, "/etc/githash")


def getGitRelease(REPO):
    def processURL(rURL):
        data = openURI(rURL)
        vers = data.url.rsplit("/", 1)[1]
        if vers == "releases" or vers == "tags":
            soup = BeautifulSoup(data, "html.parser")
            vers = soup.find_all("a", class_="Link--primary")
            for _ver in vers:
                gVer.append(stripPrefix(_ver.get_text().strip()))
        return
    gVer = []
    processURL("https://github.com/" + REPO + "/releases/latest")
    processURL("https://github.com/" + REPO + "/tags")
    return natsorted(gVer)[-1]


def getTagRelease(REPO):
    ver_list = []
    data = openURI(REPO)
    soup = BeautifulSoup(data, "html.parser")

    for link in soup.find_all("a"):
        fullname = str(link.get('href'))
        splitname = fullname.strip().split("/")[-1]
        if splitname and 'tag' in fullname:
            splitname = stripPrefix(splitname)
            if checkNum(splitname):
                ver_list.append(splitname)
    checkList("gitea version", REPO, ver_list)
    return natsorted(ver_list)[-1]


def getGitlabRelease(REPO):
    return getTagRelease(REPO + "/-/tags/")


def getGiteaRelease(REPO):
    return getTagRelease(REPO + "/tags/")


def getCargoRelease(NAME):
    data = openURI("https://lib.rs/crates/" + NAME)
    soup = BeautifulSoup(data, "html.parser")
    vers = soup.find(id="versions").find(property="softwareVersion").get_text().strip()
    return vers


def getHTTPRelease(URI, NAME, EXT):
    ver_list = []
    data = openURI(URI)
    soup = BeautifulSoup(data, "html.parser")

    for link in soup.find_all("a"):
        ver = "none"
        splitname = str(link.get('href')).strip().split("/")[-1]
        if splitname and splitname.startswith(NAME) and splitname.endswith("." + EXT):
            ver = splitname.replace(NAME, "")
            ver = ver.replace("." + EXT, "")
            ver = stripPrefix(ver)
        if checkNum(ver) and "-" not in ver:
            ver_list.append(ver)
    checkList(NAME, URI, ver_list)

    return natsorted(ver_list)[-1]


def getFTPRelease(URI, DIR, NAME, EXT):
    ftp_list = []

    def getname(recv_string):
        if recv_string.startswith(NAME) and recv_string.endswith(EXT):
            ver = recv_string.replace(NAME, "").replace(EXT, "")
            ver = stripPrefix(ver)
            ftp_list.append(ver)

    # open ftp connection
    ftp = FTP(URI)
    ftp.login()

    # list directory
    ftp.cwd(DIR)
    ftp.retrlines("NLST", callback=getname)
    ftp.quit()

    checkList(NAME, URI, ftp_list)

    return natsorted(ftp_list)[-1]


parser = ArgumentParser()
parser.add_argument(
    "-a", "--alpine", type=str, help='get latest version of alpine package "ALPINE"'
)
parser.add_argument(
    "-b", "--base", action="store_true", help="get alpine base image version"
)
parser.add_argument(
    "-m", "--myalp", type=str, help='get version of alpine in image "MYALP"'
)
parser.add_argument(
    "-e", "--edge", action="store_true", help="use alpine edge version for command"
)
parser.add_argument(
    "-d", "--docker", type=str, help='get latest docker tag of docker image "DOCKER"'
)
parser.add_argument(
    "-f", "--fhash", type=str, help='get git hash saved in docker image "FHASH"'
)
parser.add_argument(
    "-g", "--ghash", type=str, help='get latest github hash of file "GHASH"'
)
parser.add_argument(
    "-r",
    "--release",
    type=str,
    help='get latest github release of "release(USERNAME/REPO)"',
)
parser.add_argument(
    "-s",
    "--gitlab",
    type=str,
    help='get latest gitlab tag from full repo url "gitlab(USERNAME/REPO)"',
)
parser.add_argument(
    "-i",
    "--gitea",
    type=str,
    help='get latest gitea tag from full repo url "gitea(USERNAME/REPO)"',
)
parser.add_argument(
    "-c", "--cargo", type=str, help='get latest cargo release of "CARGO"'
)
parser.add_argument(
    "-l",
    "--list",
    type=str,
    help='get latest http directory/webpage release of "LIST(package name),\
URL(full url),EXT(file extension eg tar.gz)"',
)
parser.add_argument(
    "-t",
    "--ftp",
    type=str,
    help='get latest ftp directory release of "FTP(package name),\
URL(ftp server),DIR(ftp directory),EXT(file extension eg tar.gz)"',
)
parser.add_argument(
    "-u",
    "--useragent",
    type=str,
    help='user agent to use when connecting with http (github, gitlab, --list',
)

args = parser.parse_args()

if args.useragent:
    agent = args.useragent

if args.alpine:
    print(getAlpineApk(args.alpine))

if args.base:
    print(getAlpineVer())

if args.myalp:
    print(getAlpineVer(IMG=args.myalp))

if args.docker:
    print(getDockerTag(args.docker))

if args.fhash:
    print(getFileHash(args.fhash))

if args.ghash:
    print(getGitHash(args.ghash))

if args.release:
    print(getGitRelease(args.release))

if args.gitlab:
    print(getGitlabRelease(args.gitlab))

if args.gitea:
    print(getGiteaRelease(args.gitea))

if args.cargo:
    print(getCargoRelease(args.cargo))

if args.list:
    try:
        splitargs = args.list.split(",")
        nme = splitargs[0]
        uri = splitargs[1]
        ext = splitargs[2]
    except IndexError:
        raise SystemExit("Not enough , seperated arguments passed to --list")
    print(getHTTPRelease(uri, nme, ext))

if args.ftp:
    try:
        splitargs = args.ftp.split(",")
        nme = splitargs[0]
        uri = splitargs[1]
        dir = splitargs[2]
        ext = splitargs[3]
    except IndexError:
        raise SystemExit("Not enough , seperated arguments passed to --ftp")
    print(getFTPRelease(uri, dir, nme, ext))
